/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   math.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/26 18:15:02 by pmartin           #+#    #+#             */
/*   Updated: 2015/09/25 02:47:07 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmpt.h"

int simplyf(int a, int b, int c)
{
	int t;

	printf("Simplyf ::%d, %d, %d\n", a, b, c);
	if (!(a && b && a != 1 && b != 1 && c != 1
		  && a != -1 && b != -1 && c != -1))
		return (1);
	if (a < b)
	{
		t = a;
		a = b;
		b = t;
	}
	while (a % b != 0)
	{
		t = b;
		b = a % b;
		a = t;
	}
	if (c != 0)
		while (c % b)
  		{
			t = b;
			b= c % b;
			c = t;
		}
	return ((b > 0) ? b : b * -1);
}

int	tcoma(double k)
{
	int		i;
	i = k;
	k = k - i;
	if ((long int)(k * 1000000) != 0)
		return (0);
	return (1);
}

int		simplyrt(double k, t_par * par)
{
	int i;
	int b;

	par->coefsq = 1;
	if (!tcoma(k))
		return (0);
	b = 0;
	while (b == 0)
	{
		i = 0;
		b = 1;
		while (i < 4)
			if ((int)k % par->tsq[i++] == 0)
			{
				par->coefsq *= par->tsq[i - 1];
				k /= par->tsq[i - 1];
				b = 0;
			}
	}
	return (par->coefsq = (ft_sqrt(par->coefsq)));
}

double ft_sq(double k)
{
	if (k <= 46340.950001)
		return (k * k);
	else
	{
		printf("pojopjoiho[ihjokpo\n");
		exit (-666);
	}
}

double ft_sqrt(double k)
{
	int		i;
	double	rtsq;

	i = 0;
	rtsq = k;
	printf("NB ENVOYE ::%g::\n", k);
	if (k > 0)
		while (i++ < 11)
			rtsq = (0.5  * (rtsq + (k / rtsq)));
	printf("RETURN ::%g::\n", rtsq);
	return (k >= 0 ? rtsq : -666);
}
