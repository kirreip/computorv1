/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/05 19:34:26 by pmartin           #+#    #+#             */
/*   Updated: 2015/09/25 03:19:20 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmpt.h"

int	checkx(char *str)
{
	int i;

	i = 0;
	while (str[i])
		if (str[i++] == 'X')
			return (1);
	return (0);
}

int	checkeq(char *str)
{
	int i;

	i = 0;
	while (str[i])
		if (str[i++] == '=')
			return (1);
	return (0);
}

int	checkc(char c)
{
	return ((ft_isdigit(c) || c == ' ' || c == '*'
			|| c == '+' || c == '-' || c == '='
			|| c == 'X' || c == '^' || c == '.'));
}

int	checkv(char *s)
{
	int i;
	int j;

	i = 0;
	if (ft_strcmp(s, "") == 0)
		return (24);
	if (s[0] == ' ')
		return (25);
	while (s[i])
	{
		if (!(checkc(s[i])))
			return (20);
		if (s[i] == '^' && !(ft_isdigit(s[i + 1])))
			return (21);
		if (i != 0 && (s[i] == '+' || s[i] == '*' || s[i] == '=')
			&& (s[i + 1] != 32 || s[i - 1] != 32))
			return (22);
		if (s[i++] == '^')
		{
			j = i;
			while (s[j] != ' ' && s[j])
				if (s[j++] == '.')
					return (23);
			i = j - 1;
		}
	}
	return (1);
}
