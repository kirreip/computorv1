#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/05/11 18:23:17 by pmartin           #+#    #+#              #
#    Updated: 2015/09/30 02:42:38 by pmartin          ###   ########.fr        #
#                                                                              #
#******************************************************************************#


NAME = computor
CC = cc
FLAGS = -Wall -Wextra -Werror
IDIR = includes/
LIBFT = libft/
SRC = main.c \
	parser.c \
	math.c \
	check.c \
	add.c \
	res.c
OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	@make -C $(LIBFT) re
	@$(CC) -o $@ $^ $(FLAGS) $(LIBFT)libft.a -I $(IDIR)

%.o: %.c
	@$(CC) $(FLAGS) -o $@ -c $< -I $(IDIR)

clean:
	@rm -f $(OBJ)

fclean: clean
	@rm -f $(NAME)
	@make -C $(LIBFT) fclean

re: fclean all

.PHONY: all $(NAME) clean fclean re