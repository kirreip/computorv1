/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/13 01:20:47 by pmartin           #+#    #+#             */
/*   Updated: 2015/09/25 03:18:44 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmpt.h"

void	ft_error(int k)
{
	if (k == 20)
		printf("\033[33m0-9 . + - = X allowed\033[37m");
	if (k == 21)
		printf("\033[33mSpace after ^\n\033[37m");
	if (k == 22)
		printf("\033[33m!Space after + or * or =\n\033[37m");
	if (k == 23)
		printf("\033[33mBad exponent\n\033[37m");
	if (k == 24)
		printf("\033[33mNo argument\n\033[37m");
	if (k == 25)
		printf("\033[33mFirst char is a space\n\033[37m");
	if (k != 1)
		exit(666);
}

void	truc(t_par *par)
{
	int i;
	double k;
	t_result res;

	k = 0;
	i = 999;
	printf("\033[31mReduced form: ");
	while (i && !((long int)(par->abc[i] * 1000000)))
		i--;
	if (i == 0 && (long int)(par->abc[0] * 1000000) == 0)
		res0(par);
	printf("\033[37m%g * X^%d",par->abc[i], i);
	par->degre = i--;
	while (i != 0 - 1)
		if (((long int)(par->abc[i--] * 1000000)))
			printf(" + %g * X^%d",par->abc[i + 1], i + 1);
	printf(" = 0\n");
	printf("\033[31mPolynomial degree:\033[37m %d\n", par->degre);
	if (par->degre == 2)
		res2(par, &res);
	else if (par->degre == 1 || par->degre == 0)
		(par->degre == 1) ? res1(par) : res0(par);
	else
		printf("\033[31mPolinomial degree > 2\033[37m\n");
}

void	inittsq(t_par *par)
{
	par->tsq[0] = 4;
	par->tsq[1] = 9;
	par->tsq[2] = 25;
	par->tsq[3] = 49;
}

int		main(int ac, char **av)
{
	t_par par;
	int i;

	if (ac == 2)
	{
		printf("%s\n",av[1]);
		ft_error(checkv(av[1]));
		i = 0;
		while (i < 999)
			par.abc[i++] = 0;
		ft_pars(av[ac - 1], &par);
		inittsq(&par);
		truc(&par);
		return (0);
	}		
}
