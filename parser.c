/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/13 01:21:46 by pmartin           #+#    #+#             */
/*   Updated: 2015/09/03 03:00:26 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmpt.h"

void	ft_fill2(t_par *par, char *s)
{
	int i;

	i = 0;
	if (s[2] == 'X' || (s[3] == 'X' && s[2] == '-'))
		par->abc[(s[3] == '^') ? atoi(&s[4]) : 1] -= (s[0] == '-') ? -1 : 1;
	else if (s[ft_strlen(s) - 1] == 'X')
		par->abc[1] -= (s[0] == '-') ? atof(&s[2]) * -1 : atof(&s[2]);
	else if (!checkx(s))
		par->abc[0] -= (s[0] == '-') ? atof(&s[2]) * -1 : atof(&s[2]);
	else
	{
		while (s[ft_strlen(s) - 1 - i] != '^')
			i++;
		par->abc[atoi(&s[ft_strlen(s) - i])] -= (s[0] == '-') ? atof(&s[2]) * -1 : atof(&s[2]);
	}
}

void	ft_fill(t_par *par, char *s)
{
	int i;

	i = 0;
	if (s[2] == 'X')
		par->abc[(s[3] == '^') ? atoi(&s[4]) : 1] += (s[0] == '-') ? -1 : 1;
	else if (s[ft_strlen(s) - 1] == 'X')
		par->abc[1] += (s[0] == '-') ? atof(&s[2]) * -1 : atof(&s[2]);
	else if (!checkx(s))
		par->abc[0] += (s[0] == '-') ? atof(&s[2]) * -1 : atof(&s[2]);
	else
	{
		while (s[ft_strlen(s) - 1 - i] != '^')
			i++;
		par->abc[atoi(&s[ft_strlen(s) - i])] += (s[0] == '-') ? atof(&s[2]) * -1 : atof(&s[2]);
	}
}

void	ft_1pars(char *s, t_varp *varp)
{
	int i;
	int j;
	int u;

	u = 0;
	i = 0;
	j = 0;
	while (s[i] && !(((s[i + 1] == '-' || s[i + 1] == '+' || s[i + 1] == '=')
					  && s[i] == ' ' && s[i + 2] == ' ')))
		i++;
	varp->str[u] = ft_strsub(s, 0, i);
	i += 2;
	j = i;
	u ++;
	while (s[i])
	{
		while (s[i] && !(((s[i + 1] == '-'
						 || s[i + 1] == '+' || s[i + 1] == '=')
						 && s[i] == ' ' && s[i + 2] == ' ')))
			i++;
		varp->str[u] = ft_strsub(s, j  - 1, i - j  + 1);
		if (s[i])
			i += 2;
		j = i;
		u ++;
	}
}

void	ft_pars(char *s, t_par *par)
{
	t_varp varp;
	int j;
	int i = 0;

	j = 0;
	if (!checkeq(s))
		s = ft_addeq(s);
	ft_1pars(s, &varp);
	printf("Return 1Pars: \n");
	while (varp.str[i])
		printf("%s\n", varp.str[i++]);
	varp.str[0] = ft_addpl(varp.str[j]);
	printf("Return addpl:\n%s\n", varp.str[0]);
	while (varp.str[j] && varp.str[j][0] != '=')
		ft_fill(par, varp.str[j++]);
	while (varp.str[j])
		ft_fill2(par, varp.str[j++]);
}
