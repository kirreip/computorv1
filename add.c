/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/05 19:44:22 by pmartin           #+#    #+#             */
/*   Updated: 2015/09/25 03:03:46 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmpt.h"

void	resd2(t_par *par, t_result *res)
{
	int coef;

	if ((coef = simplyf(par->abc[1] * -1, simplyrt(res->delta, par)
						, par->abc[2] * 2) != 1) ||
		((coef = simplyf(simplyrt(res->delta, par), par->abc[2] * 2, 0)) != 1))
	{
		res->delta *= -1;
		printf("\033[4m\033[32mSimplified form:\n\033[37m\033[0m");
		printf("Coef = ::%d::\n RC = ::%d::\n", coef, simplyrt(2720, par));
		if (par->abc[1])
		{
			coef = simplyf(par->abc[1] * -1, par->coefsq, par->abc[2] * 2);
			printf("X1 = (%g ± %d i Sqrt(%g)) / %g\n"
				, par->abc[1] * -1 / coef, (int)par->coefsq / coef
				, res->delta / ft_sq(par->coefsq), par->abc[2] * 2 / coef);
		}
		else
		{
			printf("X1 = (%d i Sqrt(%g) / %g\n", (int)ft_sqrt(par->coefsq)
				/ coef, res->delta * -1 / coef, par->abc[2] * 2 / coef);
			printf("X1 = (%d i Sqrt(%g) / %g\n"
				, (int)ft_sqrt(par->coefsq) / coef * -1,
				res->delta / coef, par->abc[2] * 2 / coef);
		}
	}
}

void	resd(t_par *par, t_result *res)
{
	printf("\033[31mDiscriminant is strictly negative,");
	printf(" the two solutions are:\n");
	printf("\033[4m\033[32mNon-simplified form:\n\033[0m");
	if (par->abc[1])
		printf("X1 = (%g ± i * Sqrt(%g)) / (%g)\n"
			, par->abc[1] * -1, (res->delta * -1), par->abc[2] * 2);
	else
	{
		printf("X1 = (i *Sqrt(%g) / (%g)\n", (res->delta * -1),
			par->abc[2] * 2);
		printf("X2 = (-i *Sqrt(%g) / (%g)\n", (res->delta * -1),
			par->abc[2] * 2);
	}
	resd2(par, res);
}

char	*ft_addpl(char *s)
{
	int		i;
	int		k;
	char	*temp;

	i = 0;
	k = ft_strlen(s) + 2;
	//printf("Test Simplyf : %d\n", simplyf(12, 15, 24));
	if (!(temp = (char *)malloc((k) * sizeof(char))))
		return (0);
	while (i != k)
	{
		temp[k - i + 1] = s[k - i - 1];
		i++;
	}
	temp[0] = '+';
	temp[1] = ' ';
	return (temp);
}

char	*ft_addeq(char *s)
{
	int		i;
	int		k;
	char	*temp;

	i = 0;
	k = ft_strlen(s);
	if (!(temp = (char *)malloc((k + 4) * sizeof(char))))
		return (0);
	temp = ft_strcpy(temp, s);
	temp[k] = ' ';
	temp[k + 1] = '=';
	temp[k + 2] = ' ';
	temp[k + 3] = '0';
	return (temp);
}
