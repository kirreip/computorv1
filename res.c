/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   res.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/08 21:38:48 by pmartin           #+#    #+#             */
/*   Updated: 2015/09/25 03:04:57 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmpt.h"

void		simply(t_par *par, t_result *res)
{
	int coef;

	if (((coef = simplyf(par->abc[1] * -1, simplyrt(res->delta, par)
						, par->abc[2] * 2) != 1) || 
		(coef= simplyf(simplyrt(res->delta, par), par->abc[2] * 2, 0)) != 1))
	{
		printf("\033[4m\033[32mSimplified form:\n\033[37m\033[0m");
		if (par->abc[1])
		{
			coef = simplyf(par->abc[1] * -1, par->coefsq, par->abc[2] * 2);
			printf("X1 = (%g ± %dSqrt(%g)) / %g\n"
				   , par->abc[1] * -1 / coef,(int)par->coefsq / coef
				   ,res->delta / ft_sq(par->coefsq), par->abc[2] * 2 / coef);
		}
		else
		{
			printf("X1 =  Sqrt(%g) / %g\n", res->delta / (coef * coef)
				 , par->abc[2] * 2 / coef);
			printf("X2 = -Sqrt(%g) / %g\n", res->delta / (coef * coef)
				 , par->abc[2] * 2 / coef);
		}
	}
	printf("\033[32mX1 equal approximately:\033[37m %g\n"
		   , (res->x1 < res->x2) ? res->x1 : res->x2);
	printf("\033[32mX2 equal approximately:\033[37m %g\n"
		   , (res->x1 > res->x2) ? res->x1 : res->x2);
}

void		res21(t_par *par,  t_result *res)
{
		res->x1 = ((-1 * par->abc[1])
				+ ft_sqrt(res->delta)) / (2 * par->abc[2]);
		res->x2 = ((-1 * par->abc[1])
				- ft_sqrt(res->delta)) / (2 * par->abc[2]);
		printf("\033[31mDiscriminant is strictly positive,");
		printf(" the two solutions are:\n");
		printf("\033[4m\033[32mNon-simplified form:\n\033[37m\033[0m");
		if (par->abc[1])
			printf("X1 = (%g ± Sqrt(%g)) / %g\n"
				   , par->abc[1] * -1, res->delta, par->abc[2] * 2);
		else
		{
			printf("X1 = Sqrt(%g) / %g\n", res->delta, par->abc[2] * 2);
			printf("X2 = (-Sqrt(%g) / %g\n", res->delta, par->abc[2] * 2);
		}
		simply(par, res);
}

void        res0(t_par *par)
{
	if (par->abc[0] == 0)
	{
		printf("0 = 0\033[37m\n");
	}
	else
		printf("\033[31mGG:\033[37m %g = 0\n", par->abc[0]);
	exit(666);
}
void        res1(t_par *par)
{
	int coef;

	printf("\033[4m\033[32mNon-simplified form:\n\033[0m");
	printf("\033[32mThe solution is: \033[37mX = %g / %g\n"
		   , (par->abc[0] * -1), par->abc[1]);
	if ((coef = simplyf(par->abc[0], par->abc[1], 0)) != 1)
	{
		printf("\033[4m\033[32mSimplified form:\n\033[0m");
		printf("\033[32mThe solution is:\033[37m X = %g / %g\n"
			 , (par->abc[0] * -1) / coef, par->abc[1] / coef);
	}
	printf("\033[32mX equal approximately: \033[37m%g\n"
		   , (par->abc[0] * -1) / par->abc[1]);
}

void        res2(t_par *par, t_result *res)
{
	res->delta = ft_sq(par->abc[1]) - (4 * par->abc[0] * par->abc[2]);
	if ((long int)(res->delta * 1000000) == 0)
	{
		res->x1 = (-1 * par->abc[1]) / (2 * par->abc[2]);
		printf("\033[31mDiscriminant is equal to 0,");
		printf(" the only solution is:\n\033[37m");
		printf("X = %g / %g\n\033[32mApproximately:\033[37m "
			   , par->abc[1] * -1, (par->abc[2] * 2));
		printf("%g\n", res->x1);
	}
	else if ((long int)(res->delta * 1000000) > 0)
		res21(par, res);
	else if ((long int)(res->delta * 1000000) < 0)
		resd(par, res);
}
