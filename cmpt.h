/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmpt.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/13 01:22:11 by pmartin           #+#    #+#             */
/*   Updated: 2015/06/11 17:28:38 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CMPT_H
# define CMPT_H
# include <stdio.h>
# include <stdlib.h>
# include "libft/includes/libft.h"
typedef struct s_par
{
	int			degre;
	int			nbelem;
	double		abc[1000];
	int			tsq[4];
	int			coefsq;
}				t_par;
typedef struct s_varp
{
	int		i;
	int		j;
	int		u;
	char	*str[2000];
}				t_varp;
typedef struct s_result
{
	double delta;
	double x1;
	double x2;
}				t_result;
void		ft_pars(char *s, t_par *par);
double		ft_sqrt(double k);
double		ft_sq(double k);
int			checkx(char *str);
int			checkeq(char *str);
int			checkeg(double k);
int			checkv(char *s);
void		res0(t_par *par);
void		res1(t_par *par);
void		res2(t_par *par, t_result *res);
char		*ft_addeq(char *s);
char		*ft_addpl(char *s);
int			simplyf(int a, int b, int c);
int			simplyrt(double k, t_par *par);
void		resd(t_par *par, t_result *res);
#endif
